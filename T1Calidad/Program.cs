﻿using System;

namespace T1Calidad
{
    public class Program
    {
        int[] CantidadTurnos = new int[21];
        int TurnoContador;

        static void Main(string[] args)
        {
            Console.WriteLine("------ EXAMEN T1 - CALIDAD Y PRUEBAS DE SOFTWARE ------");
            Console.WriteLine("");
            Console.WriteLine("           ------ BOWLING WORLD ------");
        }

        public void Pinos(int pinos)
        {
            CantidadTurnos[TurnoContador] = pinos;
            TurnoContador++;
        }

        bool EsStrike(int i)
        {
            return CantidadTurnos[i] == 10;
        }

        bool EsSpare(int i)
        {
            return CantidadTurnos[i] + CantidadTurnos[i + 1] == 10;
        }

        int StrikeBono(int i)
        {
            return CantidadTurnos[i + 1] + CantidadTurnos[i + 2];
        }

        int SpareBono(int i)
        {
            return CantidadTurnos[i + 2];
        }

        public int Puntaje()
        {
            int puntaje = 0;
            int frameInt = 0;
            for (int frame = 0; frame < 10; frame++)
            {
                if (EsStrike(frameInt))
                {
                    puntaje += 10 + StrikeBono(frameInt);
                    frameInt += 1;
                }
                else if (EsSpare(frameInt))
                {
                    puntaje += 10 + SpareBono(frameInt);
                    frameInt += 2;
                }
                else
                {
                    puntaje += CantidadTurnos[frameInt] + CantidadTurnos[frameInt + 1];
                    frameInt += 2;
                }
            }
            return puntaje;
        }

    }
}
