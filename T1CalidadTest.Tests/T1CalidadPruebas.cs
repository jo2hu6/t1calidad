﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using T1Calidad;

namespace T1CalidadTest.Tests
{
    [TestFixture()]
    public class T1CalidadPruebas
    {
        Program lanzar;

        [SetUp]
        public void InicioJuego()
        {
            lanzar = new Program();
        }

        [Test]
        public void Test1_FullStrikes()
        {
            lanzar.Pinos(10);
            lanzar.Pinos(10);
            lanzar.Pinos(10);
            lanzar.Pinos(10);
            lanzar.Pinos(10);
            lanzar.Pinos(10);
            lanzar.Pinos(10);
            lanzar.Pinos(10);
            lanzar.Pinos(10);
            lanzar.Pinos(10); lanzar.Pinos(10); lanzar.Pinos(10);
            Assert.That(lanzar.Puntaje, Is.EqualTo(300));
        }

        [Test]
        public void Test2_FullSpares()
        {
            lanzar.Pinos(5); lanzar.Pinos(5);
            lanzar.Pinos(7); lanzar.Pinos(3);
            lanzar.Pinos(4); lanzar.Pinos(6);
            lanzar.Pinos(1); lanzar.Pinos(9);
            lanzar.Pinos(8); lanzar.Pinos(2);
            lanzar.Pinos(9); lanzar.Pinos(1);
            lanzar.Pinos(2); lanzar.Pinos(8);
            lanzar.Pinos(4); lanzar.Pinos(6);
            lanzar.Pinos(7); lanzar.Pinos(3);
            lanzar.Pinos(9); lanzar.Pinos(1); lanzar.Pinos(5);
            Assert.That(lanzar.Puntaje, Is.EqualTo(156));
        }

        [Test]
        public void Test3_JuegoStrikesSpares()
        {
            lanzar.Pinos(5); lanzar.Pinos(2);
            lanzar.Pinos(7); lanzar.Pinos(3);
            lanzar.Pinos(10);
            lanzar.Pinos(10);
            lanzar.Pinos(8); lanzar.Pinos(0);
            lanzar.Pinos(10);
            lanzar.Pinos(2); lanzar.Pinos(4);
            lanzar.Pinos(1); lanzar.Pinos(6);
            lanzar.Pinos(7); lanzar.Pinos(0);
            lanzar.Pinos(10); lanzar.Pinos(6); lanzar.Pinos(5);
            Assert.That(lanzar.Puntaje, Is.EqualTo(138));
        }

        [Test]
        public void Test4_JuegoIncompleto()
        {
            lanzar.Pinos(8); lanzar.Pinos(0);
            lanzar.Pinos(1); lanzar.Pinos(4);
            lanzar.Pinos(10);
            lanzar.Pinos(8); lanzar.Pinos(0);
            lanzar.Pinos(5); lanzar.Pinos(0);
            lanzar.Pinos(10);
            Assert.That(lanzar.Puntaje, Is.EqualTo(54));
        }

        [Test]
        public void Test5_JuegoNulo()
        {
            lanzar.Pinos(0);
            Assert.That(lanzar.Puntaje, Is.EqualTo(0));
        }

        [Test]
        public void Test6_TiroUnico()
        {
            lanzar.Pinos(1);
            Assert.That(lanzar.Puntaje, Is.EqualTo(1));
        }

        [Test]
        public void Test7_FailTotal()
        {
            lanzar.Pinos(0); lanzar.Pinos(0);
            lanzar.Pinos(0); lanzar.Pinos(0);
            lanzar.Pinos(0); lanzar.Pinos(0);
            lanzar.Pinos(0); lanzar.Pinos(0);
            lanzar.Pinos(0); lanzar.Pinos(0);
            lanzar.Pinos(0); lanzar.Pinos(0);
            lanzar.Pinos(0); lanzar.Pinos(0);
            lanzar.Pinos(0); lanzar.Pinos(0);
            lanzar.Pinos(0); lanzar.Pinos(0);
            lanzar.Pinos(0); lanzar.Pinos(0); lanzar.Pinos(0);
            Assert.That(lanzar.Puntaje, Is.EqualTo(0));
        }

        [Test]
        public void Test8_JuegoNormal1()
        {
            lanzar.Pinos(5); lanzar.Pinos(2);
            lanzar.Pinos(7); lanzar.Pinos(3);
            lanzar.Pinos(10);
            lanzar.Pinos(0); lanzar.Pinos(0);
            lanzar.Pinos(8); lanzar.Pinos(0);
            lanzar.Pinos(10);
            lanzar.Pinos(1); lanzar.Pinos(7);
            lanzar.Pinos(0); lanzar.Pinos(0);
            lanzar.Pinos(7); lanzar.Pinos(0);
            lanzar.Pinos(5); lanzar.Pinos(2); lanzar.Pinos(0);
            Assert.That(lanzar.Puntaje, Is.EqualTo(85));
        }

        [Test]
        public void Test9_JuegoNormal2()
        {
            lanzar.Pinos(7); lanzar.Pinos(2);
            lanzar.Pinos(9); lanzar.Pinos(1);
            lanzar.Pinos(10);
            lanzar.Pinos(6); lanzar.Pinos(3);
            lanzar.Pinos(10);
            lanzar.Pinos(8); lanzar.Pinos(2);
            lanzar.Pinos(7); lanzar.Pinos(3);
            lanzar.Pinos(9); lanzar.Pinos(0);
            lanzar.Pinos(8); lanzar.Pinos(1);
            lanzar.Pinos(7); lanzar.Pinos(3); lanzar.Pinos(9);
            Assert.That(lanzar.Puntaje, Is.EqualTo(150));
        }

        [Test]
        public void Test10_JuegoNormal3()
        {
            lanzar.Pinos(10);
            lanzar.Pinos(9); lanzar.Pinos(0);
            lanzar.Pinos(7); lanzar.Pinos(3);
            lanzar.Pinos(10);
            lanzar.Pinos(10);
            lanzar.Pinos(8); lanzar.Pinos(2);
            lanzar.Pinos(7); lanzar.Pinos(2);
            lanzar.Pinos(9); lanzar.Pinos(0);
            lanzar.Pinos(8); lanzar.Pinos(2);
            lanzar.Pinos(10); lanzar.Pinos(7); lanzar.Pinos(3);
            Assert.That(lanzar.Puntaje, Is.EqualTo(171));
        }

        [Test]
        public void Test11_JuegoNormal4()
        {
            lanzar.Pinos(7); lanzar.Pinos(3);
            lanzar.Pinos(8); lanzar.Pinos(2);
            lanzar.Pinos(10);
            lanzar.Pinos(10);
            lanzar.Pinos(10);
            lanzar.Pinos(8); lanzar.Pinos(2);
            lanzar.Pinos(7); lanzar.Pinos(3);
            lanzar.Pinos(9); lanzar.Pinos(0);
            lanzar.Pinos(10);
            lanzar.Pinos(10); lanzar.Pinos(10); lanzar.Pinos(9);
            Assert.That(lanzar.Puntaje, Is.EqualTo(220));
        }

        [Test]
        public void Test12_JuegoNormal5()
        {
            lanzar.Pinos(8); lanzar.Pinos(0);
            lanzar.Pinos(7); lanzar.Pinos(0);
            lanzar.Pinos(5); lanzar.Pinos(3);
            lanzar.Pinos(9); lanzar.Pinos(1);
            lanzar.Pinos(9); lanzar.Pinos(1);
            lanzar.Pinos(10);
            lanzar.Pinos(8); lanzar.Pinos(0);
            lanzar.Pinos(5); lanzar.Pinos(1);
            lanzar.Pinos(3); lanzar.Pinos(7);
            lanzar.Pinos(9); lanzar.Pinos(0); lanzar.Pinos(0);
            Assert.That(lanzar.Puntaje, Is.EqualTo(122));
        }

        [Test]
        public void Test13_JuegoNormal6()
        {
            lanzar.Pinos(8); lanzar.Pinos(2);
            lanzar.Pinos(9); lanzar.Pinos(0);
            lanzar.Pinos(4); lanzar.Pinos(4);
            lanzar.Pinos(7); lanzar.Pinos(2);
            lanzar.Pinos(9); lanzar.Pinos(0);
            lanzar.Pinos(10);
            lanzar.Pinos(10);
            lanzar.Pinos(8); lanzar.Pinos(0);
            lanzar.Pinos(3); lanzar.Pinos(5);
            lanzar.Pinos(9); lanzar.Pinos(1); lanzar.Pinos(7);
            Assert.That(lanzar.Puntaje, Is.EqualTo(133));
        }

        [Test]
        public void Test14_StrikeIncompleto()
        {
            lanzar.Pinos(10);
            lanzar.Pinos(10);
            lanzar.Pinos(10);
            lanzar.Pinos(10);
            Assert.That(lanzar.Puntaje, Is.EqualTo(90));
        }

        [Test]
        public void Test15_StrikeSparesIncompleto()
        {
            lanzar.Pinos(5); lanzar.Pinos(2);
            lanzar.Pinos(7); lanzar.Pinos(3);
            lanzar.Pinos(10);
            lanzar.Pinos(10);
            lanzar.Pinos(8); lanzar.Pinos(0);
            lanzar.Pinos(10);
            Assert.That(lanzar.Puntaje, Is.EqualTo(91));
        }
    }
}
